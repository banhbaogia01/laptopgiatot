import { RichText, Link } from 'prismic-dom';
import { linkResolver } from 'route';
import { useTheme } from '@mui/material/styles';
import { makeStyles } from '@mui/styles';


// -- HTML Serializer
const htmlSerializer = (type: any, element: any, content: any, children: any, key: any) => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const MyTheme = useTheme();
  const useStyles = makeStyles({
    h3: {
      ...MyTheme.typography.h6
    },
    p: {
      ...MyTheme.typography.body1,
      margin: 0
    }
  });
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const classes = useStyles();
  
  switch(type) {
    case RichText.Elements.heading1:

      return `<h1 class="${classes.h3}">${children.join('')}</h1>`;
    case RichText.Elements.heading2: return `<h2>${children.join('')}</h2>`;
    case RichText.Elements.heading3: return `<h3 class="${classes.h3}">${children.join('')}</h3>`;
    case RichText.Elements.heading4: return `<h4>${children.join('')}</h4>`;
    case RichText.Elements.heading5: return `<h5>${children.join('')}</h5>`;
    case RichText.Elements.heading6: return `<h6>${children.join('')}</h6>`;
    case RichText.Elements.paragraph: return `<p class="${classes.p}">${children.join('')}</p>`;
    case RichText.Elements.preformatted: return `<pre>${children.join('')}</pre>`;
    case RichText.Elements.strong: return `<strong>${children.join('')}</strong>`;
    case RichText.Elements.em: return `<em>${children.join('')}</em>`;
    case RichText.Elements.listItem: return `<li>${children.join('')}</li>`;
    case RichText.Elements.oListItem: return `<li>${children.join('')}</li>`;
    case RichText.Elements.list: return `<ul>${children.join('')}</ul>`;
    case RichText.Elements.oList: return `<ol>${children.join('')}</ol>`;
    case RichText.Elements.image:
      const linkUrl = element.linkTo ? Link.url(element.linkTo, module.exports.linkResolver) : null;
      const linkTarget = element.linkTo && element.linkTo.target ? `target="${element.linkTo.target}" rel="noopener"` : '';
      const wrapperClassList = [element.label || '', 'img-article block-img'];
      const img = `<img class="img-article" src="${element.url}" alt="${element.alt || ''}" copyright="${element.copyright || ''}">`;
      return (`
        <p class="${wrapperClassList.join(' ')}">
          ${linkUrl ? `<a ${linkTarget} href="${linkUrl}">${img}</a>` : img}
        </p>
      `);
    case RichText.Elements.embed:
      return (`
        <div data-oembed="${element.oembed.embed_url}"
          data-oembed-type="${element.oembed.type}"
          data-oembed-provider="${element.oembed.provider_name}"
        >
          ${element.oembed.html}
        </div>
      `);
    case RichText.Elements.hyperlink:
      const target = element.data.target ? `target="${element.data.target}" rel="noopener"` : '';
      const hyperLinkUrl = Link.url(element.data, linkResolver as any);
      return `<a ${target} href="${hyperLinkUrl}">${children.join('')}</a>`
    case RichText.Elements.label:
      const label = element.data.label ? ` class="${element.data.label}"` : '';
      return `<span ${label}>${children.join('')}</span>`;
    case RichText.Elements.span: return content ? content.replace(/\n/g, "<br />") : '';
    default: return null;
  }
};

export default htmlSerializer;