import NextLink from 'next/link';

// -- Access Token if the repository is not public
// Generate a token in your dashboard and configure it here if your repository is private
export const accessToken = ''

// -- Link resolution rules
// Manages the url links to internal Prismic documents
export const linkResolver = (doc: any) => {
  if (doc.type === 'landing-page' && doc.uid !== 'trang-chu') {
    return `/${doc.uid}`
  }
  
  if (doc.type === 'san_pham') {
    return `/san-pham/${doc.uid}`;
  }

  return '/';
}

// Additional helper function for Next/Link components
export const hrefResolver = (doc: any) => {
  if (doc.type === 'landing_page' && doc.uid !== 'trang-chu') {
    return '/[uid]'
  }

  if (doc.type === 'san_pham') {
    return '/san-pham/[uid]';
  }

  return '/';
}

export const customLink = (link: any, children: any, index: any) => (
  <NextLink
    key={index}
    href={hrefResolver(link) as any}
    as={linkResolver(link)}
  >
    {children}
  </NextLink>
);
