import Script from "next/script";
import React from "react";

const FacebookChat = () => (
  <>
    {/* Messenger Chat plugin Code */}
    <div id="fb-root"></div>

    {/* Your Chat plugin code */}
    <div id="fb-customer-chat" className="fb-customerchat"></div>

    <Script
      id="fb-boxchat-init"
      strategy="lazyOnload"
      dangerouslySetInnerHTML={{
        __html: `var chatbox = document.getElementById('fb-customer-chat');
        chatbox.setAttribute("page_id", "2081048698832328");
        chatbox.setAttribute("attribution", "biz_inbox");
      `}}
    />
  
    <Script
      id="fb-boxchat"
      strategy="lazyOnload"
      dangerouslySetInnerHTML={{
        __html: `
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v12.0'
          });
        };
  
        (function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));`
      }}
    />
  </>
);

export default FacebookChat;
