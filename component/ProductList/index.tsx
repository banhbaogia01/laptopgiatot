
import { RichText } from "prismic-dom";
import React from "react";
import htmlSerializer from "lib/html-serializer";
import { customLink, linkResolver } from "route";
import NumberFormat from 'react-number-format';

import Button from "@mui/material/Button";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";

const ProductList = (props: any) => {
  return (
    <Grid container spacing={4}>
      {props.results.map((card: any) => {
        return (
        <Grid item key={card.id} xs={12} sm={6} md={4}>
          <Paper
            elevation={5}
            sx={{ height: '100%', display: 'flex', flexDirection: 'column' }}
          >
            <CardMedia
              component="img"
              image={card.data.anh_dai_dien[0].hinh_anh.url}
              alt={card.data.anh_dai_dien[0].hinh_anh.mo_ta}
              sx={{
                maxHeight: 270
              }}
            />
            <CardContent sx={{ flexGrow: 1 }}>
              <Typography gutterBottom variant="h5" component="h2">
                {RichText.asText(card.data.ten_san_pham)}
              </Typography>
              <Typography gutterBottom variant="h6" component="h2" textAlign="right">
                <NumberFormat value={card.data.gia_san_pham} thousandSeparator={true} displayType="text" />
              </Typography>
              <Typography
                component="div"
                variant="body1"
                dangerouslySetInnerHTML={{
                  __html: RichText.asHtml(card.data.tom_tat_san_pham, linkResolver, htmlSerializer)
                }}
              >
              </Typography>
            </CardContent>
            <CardActions>
            {customLink(
              card,
              (
                <Button size="small">Xem thêm</Button>
              ),
              card.id
            )}
            </CardActions>
          </Paper>
        </Grid>
      )
    })}
    </Grid>
  );
}

export default ProductList;