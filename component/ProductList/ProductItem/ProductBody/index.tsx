// import { Grid, Typography } from "@mui/material";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import { RichText } from "prismic-dom";
import React from "react";
import htmlSerializer from "lib/html-serializer";
import { linkResolver } from "route";

const ProductBody = ({ props }: any) => {
  return (
    <Grid container marginTop={9}>
      <Typography
        component="div"
        variant="body1"
        dangerouslySetInnerHTML={{
          __html: RichText.asHtml(props.primary.noi_dung, linkResolver, htmlSerializer)
        }}
      >
      </Typography>
    </Grid>
  );
}

export default ProductBody;