import Image from 'next/image';
// import { Grid, Box, CardMedia, Typography, Paper } from "@mui/material";
import { RichText } from "prismic-dom";
import React from "react";
import htmlSerializer from "lib/html-serializer";
import { linkResolver } from "route";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';

const PrevArrow = (props: any) => {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, left: 2, zIndex: 1 }}
      onClick={onClick}
    />
  );
}

const NextArrow = (props: any) => {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, right: 2 }}
      onClick={onClick}
    />
  );
}

const ProductHeader = ({ props }: any) => {
  const settings = {
    dots: false,
    autoplay: true,
    pauseOnFocus: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />
  };

  return (
    <Grid container spacing={{ xs: 1, md: 5 }}>
      <Grid container item md={6} key={1} direction="row" justifyContent="space-around">
        <Paper elevation={5} sx={{ width: '100%' }}>
          <Slider {...settings}>
            {props.anh_dai_dien.length > 0 && props.anh_dai_dien.map((img: any, idx: number) => (
              <Image
                key={idx}
                src={img.hinh_anh.url}
                alt={img.hinh_anh.alt}
                width={500}
                height={450}
                layout="responsive"
                quality={65}
                objectFit="cover"
              />
            ))}
          </Slider>
        </Paper>
      </Grid>
      <Grid item md={6} key={1}>
        <Typography gutterBottom variant="h4" component="h1" fontWeight="bold">
          {RichText.asText(props.ten_san_pham)}
        </Typography>
        <Typography
          component="div"
          variant="body1"
          dangerouslySetInnerHTML={{
            __html: RichText.asHtml(props.tom_tat_san_pham, linkResolver, htmlSerializer)
          }}
        >
        </Typography>
      </Grid>
    </Grid>
  );
}

export default ProductHeader;