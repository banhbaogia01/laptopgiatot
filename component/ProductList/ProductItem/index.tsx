import { Box } from "@mui/material";
import React from "react";
import ProductHeader from "./ProductHeader";
import ProductBody from "./ProductBody";

const ProductItem = ({ props }: any) => {
  return (
    <Box marginTop={8}>
      <ProductHeader props={props}/>
      <ProductBody props={props.body[0]} />
    </Box>
  );
}

export default ProductItem;
