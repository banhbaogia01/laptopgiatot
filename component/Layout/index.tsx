import React from 'react';
import Footer from './Footer';
import Head from './Head';
import TopBar from './TopBar';

const Layout = ({ layoutProps, children }: any) => {
  return (
    <>
      <Head {...layoutProps} />
      {/* <!-- Google Tag Manager (noscript) --> */}
      <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MFBQ5JS" height="0" width="0" style={{ display: "none", visibility: "hidden"}}></iframe>
      </noscript>
      {/* <!-- End Google Tag Manager (noscript) --> */}
      <TopBar props={layoutProps} />
      <main>{children}</main>
      <Footer {...layoutProps} />
    </>
  );
};

export default Layout;