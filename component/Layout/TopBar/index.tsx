import AppBar from "@mui/material/AppBar";
import Link from "@mui/material/Link";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import React from "react";
import { Laptop } from "@mui/icons-material";

const TopBar = ({ props }: any) => {
  return (
    <AppBar position="relative">
      <Link href="/" color="inherit" underline="none">
        <Toolbar>
          <Laptop sx={{ mr: 2 }} />
          <Typography variant="h6" color="inherit" noWrap>
            {props.website_name}
          </Typography>
        </Toolbar>
      </Link>
    </AppBar>
  )
}

export default TopBar;