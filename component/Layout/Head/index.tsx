import NextHead from 'next/head';
import Script from 'next/script';

const Head = (props: any) => {
  return (
    <NextHead>
      <title>{props.website_name}</title>

      {/* <!-- Meta --> */}
      <meta charSet="utf-8" />
      <meta httpEquiv="X-UA-Compatible"    content="IE=edge" />
      <meta name="viewport"                 content="width=device-width, initial-scale=1.0" />
      <meta name="description"              content={props.meta_description} />
      <meta name="author"                   content={props.meta_title} />
      <meta name="msapplication-TileColor"  content="#da532c" />
      <meta name="theme-color"              content="#ffffff" />

      {/* <meta property="fb:app_id"      content="2608283835877095" />  */}
      <meta property="og:site_name"   content={props.meta_title} />
      <meta property="og:url"         content="https://laptopgiatot.penbistore.com" />
      <meta property="og:title"       content={props.meta_title} />
      <meta property="og:description" content={props.meta_description} />
      <meta property="og:type"        content="content" />
      <meta property="og:image"       content={props.favicon.url} />

      <link rel="alternate"     href="https://laptopgiatot.penbistore.com" hrefLang="vi" />
      {props.favicon.url && (
        <link rel="shortcut icon" href={props.favicon.url} type="image/x-icon" />
      )}

      {!props.favicon.url && (
        <>
          <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png" />
          <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png" />
          <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png" />
          <link rel="manifest" href="/favicon/site.webmanifest" />
          <link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5" />
        </>
      )}
      
      <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    </NextHead>
  );
}

export default Head;
