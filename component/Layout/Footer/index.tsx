import React from "react";
import { Box, Stack, Typography } from "@mui/material";

const Footer = (props: any) => {
  return (
    <Box
      component="footer"
      sx={{
        bgcolor: 'background.paper',
        pt: 30,
        pb: 10,
      }}
    >
      <Stack spacing={1}>
        <Typography
          variant="subtitle2"
          align="center"
          color="text.secondary"
          component="p"
          sx={{ padding: 0 }}
        >
          Copyright © {new Date().getFullYear()}
        </Typography>
        <Typography variant="subtitle2" align="center" gutterBottom>
          Developed by T3H4
        </Typography>
      </Stack>
    </Box>
  )
}

export default Footer;