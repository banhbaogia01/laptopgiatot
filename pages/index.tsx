import React from 'react';
import Prismic from '@prismicio/client'
import { Client } from 'lib/prismic-configuration';

import { useRouter } from 'next/router';
import Layout from 'component/Layout';
import ProductList from 'component/ProductList';
import Container from '@mui/material/Container';

export async function getStaticProps() {
  const client = Client();

  const { data: page_props } = await client.getByUID('landing-page', 'trang-chu', {});
  const page_content = await client.query(
    Prismic.Predicates.any('document.type', ['san_pham']),
    { orderings : '[document.last_publication_date desc]' });

  return {
    props: {
      page_props,
      page_content
    },
    revalidate: 60,
  }
}

function Home(props: any) {
  const router = useRouter()
  
  if (router.isFallback) {
    return <div>Loading...</div>
  }

  return (
    <Layout layoutProps={props.layout}>
      <Container>
        <h1>{props.page_props.page_name}</h1>
        <ProductList {...props.page_content} />
      </Container>
    </Layout>
  )
}

export default Home;
