import React from 'react';
import dynamic from 'next/dynamic'

import Prismic from '@prismicio/client';
import Container from '@mui/material/Container';
import Layout from 'component/Layout';
import { Client } from 'lib/prismic-configuration';

export async function getStaticPaths() {
  const client = Client();
  const { results: products } = await client.query(
    Prismic.Predicates.any('document.type', ['san_pham']),
    { orderings : '[document.last_publication_date desc]' });
  return {
    paths: products?.map((prod: any) => `/san-pham/${prod.uid}`) || [],
    fallback: 'blocking',
  }
}

export async function getStaticProps({ params }: any) {
  const client = Client();

  const { data: page_props } = await client.getByUID('san_pham', params.uid, {});

  return {
    props: {
      page_props,
    },
    revalidate: 60,
  }
}


function Product(props: any) {
  const ProductItem: any = dynamic(() => import('component/ProductList/ProductItem'));
  return (
    <Layout layoutProps={props.layout}>
      <Container maxWidth="md">
        <ProductItem props={props.page_props} />
      </Container>
    </Layout>
  )
}

export default Product;

