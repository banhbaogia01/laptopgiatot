import React from 'react';
import type { AppProps } from 'next/app';
import Script from 'next/script';
import { CacheProvider, ThemeProvider } from '@emotion/react';
import createEmotionCache from 'lib/createEmotionCache';
import { Client } from 'lib/prismic-configuration';
import { createTheme, CssBaseline } from '@mui/material';
import 'styles/globals.css'

interface MyAppProps extends AppProps {
  layout: any,
  emotionCache?: any,
}

const theme = createTheme();
const clientSideEmotionCache = createEmotionCache();

MyApp.getInitialProps = async () => {
  const client = Client();
  const { data } = await client.getSingle('global-setting', {});
  return {
    layout: data,
  };
}

function MyApp({ Component, emotionCache = clientSideEmotionCache, pageProps, layout }: MyAppProps) {
  return (
    <CacheProvider value={emotionCache}>
      {/* <!-- Global site tag (gtag.js) - Google Analytics --> */}
      <Script id="gtag" strategy="afterInteractive" async src="https://www.googletagmanager.com/gtag/js?id=G-HB2P9XRQ0V" />
      <Script
        id="gtag-js"
        strategy="afterInteractive"
        dangerouslySetInnerHTML={{
          __html: `window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', 'G-HB2P9XRQ0V');`
        }}
      />

      {/* <!-- Google Tag Manager --> */}
      <Script
        id="gg-tag-manager"
        strategy="afterInteractive"
        dangerouslySetInnerHTML={{
          __html: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
          new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
          j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
          'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
          })(window,document,'script','dataLayer','GTM-MFBQ5JS');`
        }}
      />
      {/* <!-- End Google Tag Manager --> */}

      <Script id="gg-adsense" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-8008142843822067" crossOrigin="anonymous" />

      {/* Facebook chat */}
      <Script
        id="fb-boxchat-init"
        strategy="lazyOnload"
        dangerouslySetInnerHTML={{
          __html: `var chatbox = document.getElementById('fb-customer-chat');
          chatbox.setAttribute("page_id", "2081048698832328");
          chatbox.setAttribute("attribution", "biz_inbox");
        `}}
      />
    
      <Script
        id="fb-boxchat"
        strategy="lazyOnload"
        dangerouslySetInnerHTML={{
          __html: `
          window.fbAsyncInit = function() {
            FB.init({
              xfbml            : true,
              version          : 'v12.0'
            });
          };
    
          (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
            fjs.parentNode.insertBefore(js, fjs);
          }(document, 'script', 'facebook-jssdk'));`
        }}
      />
      <ThemeProvider theme={theme}>
        {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
        <CssBaseline />
        <Component {...pageProps} layout={layout} />
      </ThemeProvider>
    </CacheProvider>
  )
}

export default MyApp
